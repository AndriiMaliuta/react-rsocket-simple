import React, { useState, useEffect } from 'react';
import { RSocketClient, MAX_STREAM_ID } from 'rsocket-core';
import RSocketWebSocketClient from 'rsocket-websocket-client';
import { Flowable, Single } from 'rsocket-flowable';
import {
  PartialResponder,
  ReactiveSocket,
  Payload,
  ISubscription,
} from 'rsocket-types';

const RSockClient = () => {
  class SymmetricResponder {
    fireAndForget(payload) {
      logRequest('fnf', payload);
    }
    requestResponse(payload) {
      logRequest('requestResponse', payload);
      return Single.error(new Error());
    }
  }

  function logRequest(type, payload) {
    console.log(
      `Client got ${type} with payload: data: ${payload.data || 'null'},
        metadata: ${payload.metadata || 'null'}`
    );
  }

  function getClientTransport(protocol, options) {
    return new RSocketWebSocketClient({
      url: 'ws://' + options.host + ':' + options.port,
      wsCreator: (url) => {
        return new WebSocket(url);
      },
    });
  }

  function runOperation(socket, options) {
    return new Promise((resolve, reject) => {
      let subscription;
      doOperation(socket, options.operation, options.payload).subscribe({
        onComplete() {
          console.log('onComplete()');
          resolve();
        },
        onError(error) {
          console.log('onError(%s)', error.message);
          reject(error);
        },
        onNext(payload) {
          console.log('onNext(%s)', payload.data);
        },
        onSubscribe(_subscription) {
          subscription = _subscription;
          subscription.request(MAX_STREAM_ID);
        },
      });
    });
  }

  function doOperation(socket, operation, payload) {
    switch (operation) {
      case 'none':
        return Flowable.never();
      case 'stream':
      default:
        console.log(`Requesting stream with payload: ${payload}`);
        return socket.requestStream({
          data: payload,
          metadata: '',
        });
    }
  }

  function connect(protocol, options) {
    const client = new RSocketClient({
      setup: {
        dataMimeType: 'text/plain',
        keepAlive: 1000000, // avoid sending during test
        lifetime: 100000,
        metadataMimeType: 'text/plain',
      },
      responder: new SymmetricResponder(),
      transport: getClientTransport(protocol, options),
    });
    return client.connect();
  }

  async function run(options) {
    const serverOptions = {
      host: options.host,
      port: options.port,
    };

    console.log(`Client connecting to ${options.host}:${options.port}`);

    const socket = await connect(options.protocol, serverOptions);
    socket.connectionStatus().subscribe((status) => {
      console.log('Connection status:', status);
    });

    return runOperation(socket, options);
  }

  useEffect(() => {
    async function init() {
      Promise.resolve(
        run({
          host: '127.0.0.1',
          port: 7878,
        })
      ).then(
        () => {
          console.log('exit');
          process.exit(0);
        },
        (error) => {
          console.error(error.stack);
          process.exit(1);
        }
      );
    }
    init();
  }, []);

  return <div></div>;
};

export default RSockClient;
class SymmetricResponder {
  fireAndForget(payload) {
    logRequest('fnf', payload);
  }
  requestResponse(payload) {
    logRequest('requestResponse', payload);
    return Single.error(new Error());
  }
}

function logRequest(type, payload) {
  console.log(
    `Client got ${type} with payload: data: ${payload.data || 'null'},
        metadata: ${payload.metadata || 'null'}`
  );
}

function getClientTransport(protocol, options) {
  return new RSocketWebSocketClient({
    url: 'ws://' + options.host + ':' + options.port,
    wsCreator: (url) => {
      return new WebSocket(url);
    },
  });
}

function runOperation(socket, options) {
  return new Promise((resolve, reject) => {
    let subscription;
    doOperation(socket, options.operation, options.payload).subscribe({
      onComplete() {
        console.log('onComplete()');
        resolve();
      },
      onError(error) {
        console.log('onError(%s)', error.message);
        reject(error);
      },
      onNext(payload) {
        console.log('onNext(%s)', payload.data);
      },
      onSubscribe(_subscription) {
        subscription = _subscription;
        subscription.request(MAX_STREAM_ID);
      },
    });
  });
}

function doOperation(socket, operation, payload) {
  switch (operation) {
    case 'none':
      return Flowable.never();
    case 'stream':
    default:
      console.log(`Requesting stream with payload: ${payload}`);
      return socket.requestStream({
        data: payload,
        metadata: '',
      });
  }
}

function connect(protocol, options) {
  const client = new RSocketClient({
    setup: {
      dataMimeType: 'text/plain',
      keepAlive: 1000000, // avoid sending during test
      lifetime: 100000,
      metadataMimeType: 'text/plain',
    },
    responder: new SymmetricResponder(),
    transport: getClientTransport(protocol, options),
  });
  return client.connect();
}

async function run(options) {
  const serverOptions = {
    host: options.host,
    port: options.port,
  };

  console.log(`Client connecting to ${options.host}:${options.port}`);

  const socket = await connect(options.protocol, serverOptions);
  socket.connectionStatus().subscribe((status) => {
    console.log('Connection status:', status);
  });

  return runOperation(socket, options);
}

useEffect(() => {
  async function init() {
    Promise.resolve(
      run({
        host: '127.0.0.1',
        port: 7878,
      })
    ).then(
      () => {
        console.log('exit');
        process.exit(0);
      },
      (error) => {
        console.error(error.stack);
        process.exit(1);
      }
    );
  }
  init();
}, []);
