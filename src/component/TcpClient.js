import React from 'react';
import { RSocketClient, Payload } from 'rsocket-core';
import RSocketTCPClient from 'rsocket-tcp-client';

const TcpClient = () => {
  async function createClient(options) {
    const client = new RSocketClient({
      setup: {
        dataMimeType: 'text/plain',
        keepAlive: 1000000, // avoid sending during test
        lifetime: 100000,
        metadataMimeType: 'text/plain',
      },
      transport: new RSocketTCPClient({
        host: options.host,
        port: options.port,
      }),
    });

    return await client.connect();
  }
  async function run() {
    const rsocket = await createClient({
      host: '127.0.0.1',
      port: 7878,
    });
    const payload = new Payload();
    rsocket.fireAndForget(payload);
  }

  async function createClient(options) {
    const client = new RSocketClient({
      setup: {
        dataMimeType: 'text/plain',
        keepAlive: 1000000, // avoid sending during test
        lifetime: 100000,
        metadataMimeType: 'text/plain',
      },
      transport: new RSocketTCPClient({
        host: options.host,
        port: options.port,
      }),
    });

    return await client.connect();
  }
  async function run() {
    const rsocket = await createClient({
      host: '127.0.0.1',
      port: 7878,
    });
    const payload = new Payload();

    rsocket.fireAndForget(payload);
  }

  run();

  return <div>TCP</div>;
};

export default TcpClient;
