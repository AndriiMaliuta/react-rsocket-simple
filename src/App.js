import React, { useState, useEffect } from 'react';
import './App.css';
import WebSocket from 'ws';
import {
  RSocketClient,
  JsonSerializer,
  IdentitySerializer,
} from 'rsocket-core';

import RSocketWebSocketClient from 'rsocket-websocket-client';

const transport = new RSocketWebSocketClient({
  url: 'ws://localhost:7878',
});

const client = new RSocketClient({
  // send/receive JSON objects instead of strings/buffers
  serializers: JsonSerializer,
  setup: {
    // ms btw sending keepalive to server
    keepAlive: 60000,
    // ms timeout if no keepalive response
    lifetime: 180000,
    // format of `data`
    dataMimeType: 'application/json',
    // format of `metadata`
    metadataMimeType: 'application/json',
  },
  transport,
});

client.connect().subscribe({
  onMessage: (msg) => {
    console.log(msg);
  },
  onComplete: (socket) => {
    socket.requestStream({
      data: { message: 'hello from javascript!' },
      metadata: null,
    });
  },
  onError: (error) => {
    console.log('got error');
    console.error(error);
  },
  onSubscribe: (cancel) => {
    /* call cancel() to abort */
    console.log('cancel!');
    // console.log(cancel);
    // cancel.cancel();
  },
});

function App() {
  return <div className="App">Sock</div>;
}

export default App;
